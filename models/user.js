const mongoose = require('mongoose')
const { Schema } = mongoose

const userSchema = Schema({
  username: String,
  password: String,
  name: String,
  surname:String,
  position:String,
  institution: {
    type: Schema.Types.ObjectId,
    ref: 'Institution'
  },
  
})

module.exports = mongoose.model('User', userSchema)
