const mongoose = require('mongoose')
const { Schema } = mongoose

const roomSchema = Schema({
  code: String,
  name: String,
  desc: String,
  building: {
    type: Schema.Types.ObjectId,
    ref: 'Building'
  },
  seat: Number
})

module.exports = mongoose.model('Room', roomSchema)
