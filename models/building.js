const mongoose = require('mongoose')
const { Schema } = mongoose
const buildingSchema = Schema({
  code: String,
  name: String
})
module.exports = mongoose.model('Building', buildingSchema)
